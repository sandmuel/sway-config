Akoziland is a set of configurations for the sway desktop environment, with
the intention of making things look warm & cozy the setup script will
symlink all configs to the appropriate locations.

License at root applies only to my own configs (subdirectories without their own license).