{
    /*******/
    /* Bar */
    /*******/

    "layer": "top",
    "position": "top",
    "modules-left": ["sway/workspaces","custom/scratchpad","custom/spacer","tray","custom/spacer","sway/mode"],
    "modules-center": ["sway/window"],
    "modules-right": ["network","pulseaudio","backlight","battery","battery#bat2","custom/spacer","clock","custom/spacer"],
    "custom/spacer": {
        "format": " "
    },
    "sway/mode": {
        "format": " {}"
    },
    "sway/window": {
        "max-length": 48,
        "tooltip": false
    },

    /**************/
    /* Workspaces */
    /**************/

    "sway/workspaces": {
	"format": "{icon}",
	"format-icons": {
	    "1": "I",
	    "2": "II",
	    "3": "III",
	    "4": "IV",
	    "5": "V",
	    "6": "VI",
	    "7": "VII",
	    "8": "VIII",
	    "9": "IX",
	    "10": "X"
	}
    },
    "custom/scratchpad": {
        "interval": 5,
        "exec": "swaymsg -t get_tree | jq --unbuffered --compact-output '(recurse(.nodes[]) | select(.name == \"__i3_scratch\") | .focus) as $scratch_ids | [..  | (.nodes? + .floating_nodes?) // empty | .[] | select(.id |IN($scratch_ids[]))] as $scratch_nodes | if ($scratch_nodes|length) > 0 then { text: \"\\($scratch_nodes | length)\", tooltip: $scratch_nodes | map(\"\\(.app_id // .window_properties.class) (\\(.id)): \\(.name)\") | join(\"\\n\") } else empty end'",
        "format": "🗗",
        "on-click": "exec swaymsg 'scratchpad show'",
        "on-click-right": "exec swaymsg 'move scratchpad'",
        "tooltip": false
    },

    /*****************/
    /* Other Banners */
    /*****************/

    "tray": {
        "spacing": 10
    },
    "clock": {
        "interval": 60,
        "format": " {:%R}",
        "format-alt": " {:%H:%M %d}",
        "tooltip-format": "<span color='#ff8844'><big>{:%Y %B}</big></span>\n<span color='#ff6622'><tt><small>{calendar}</small></tt></span>"
    },
    "backlight": {
        "format": "{icon}",
        "format-icons": ["","","","","","","","","","","","","","",""],
        "tooltip-format": "{percent}%"
    },
    "battery": {
        "interval": 8,
        "format": "{icon}",
        "format-charging": "{icon} +",
        "format-plugged": "{icon}",
        "format-alt": "{icon} {capacity}%",
        "format-icons": ["󰂎","󰁺","󰁻","󰁼","󰁽","󰁾","󰁿","󰂀","󰂁","󰂂","󱈏"]
    },
    "battery#bat2": {
        "bat": "BAT2"
    },
    "network": {
        "format": "{icon}",
	"format-icons": ["", "", "", ""],
	"on-click-right": "~/.config/rofi/network_manager/rofi-network-manager.sh",
	"tooltip-format": "{signalStrength}%"
    },
    "pulseaudio": {
        "format": "{icon}",
        "format-muted": "󰝟",
        "format-icons": ["","","",""],
        "scroll-step": 10,
        "on-click-right": "pavucontrol",
        "tooltip-format": "{volume}%"
    }
}
