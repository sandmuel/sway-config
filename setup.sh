collection=.akoziland

git clone https://gitlab.com/sandmuel/sway-config ~/$collection-update
sudo rm ~/$collection -r
mv ~/$collection-update ~/$collection

echo "Setting the default browser..."
xdg-settings set default-web-browser io.gitlab.librewolf-community

echo "Discarding present configurations..."
rm ~/.config/waybar
rm ~/.config/sway
rm ~/.config/rofi
rm ~/.config/dunst

echo "Symlinking coziland configurations..."
ln -s ~/$collection/waybar ~/.config/waybar
ln -s ~/$collection/sway ~/.config/sway
ln -s ~/$collection/rofi/launcher ~/.config/rofi
ln -s ~/$collection/rofi/network_manager ~/.config/rofi/network_manager
ln -s ~/$collection/dunst ~/.config/dunst
